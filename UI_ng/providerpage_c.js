﻿/* The main aim of the page is to do all the crud operation by the provider */

// Making the app modeule
var app = angular.module("ngproviderpage", ['ngFileUpload']);

// Starting the controller action
app.controller("profileupdate", ['$scope', 'myService', 'Upload', '$timeout', '$http', function ($scope, myService, Upload, $timeout, $http) {
        var cityandstate = null;
        // Getting the basic details fo the provider. The provider details data gets automatically filled on page load
        $http.get('/api/v1/provider_api/api/getProviderDetails').then(function (response) {
            $scope.profile = response.data;
            myService.set(response.data.userid); // setting userid 
            
            if (response.data.State) {
                var mycities = [];
                
                // Get list of cities for the state
                $http.get('/api/v1/comman_api/getCitesofState/' + response.data.State).then(function (res) {
                    if (res.data == "error") {
                        alert("error");
                    } else {
                        $scope.cities = res.data[0].city.split(",");
                    }
                })
            }
            
            refresh(); // get list of course
        })
        
        // Get the list of all states
        $http.get('/api/v1/comman_api/getStates').then(function (response) {
            var statelist = [];
            cityandstate = response.data;
            $.each(response.data, function (i, v) {
                statelist.push(v.State);
            });
            $scope.states = statelist;
             
        })
        
        // Get cities of the passed state name
        $scope.getCitiesforstate = function (state) {
            $.each(cityandstate, function (i, v) {
                if (v.State === state) {
                    var cities = v.city.split(",");
                    $scope.cities = cities;
                }
            })
        }
        // Onload Data ends <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        
        // Geting list of courses
        var refresh = function () {
            var providerid = myService.get();
            $http.get('/api/v1/provider_api/api/getCourseList/' + providerid).then(function (res) {
                $scope.providerallitems = res.data;
                console.log(res.data);
                $('#editdataid').hide();
                $('#courselist').show();
            })
        }
        refresh();
        
        // Profile submission will basically update the doc
        $scope.submitprofile = function (profile) {
            profile.id = myService.get();
            $http.post('/api/v1/provider_api/api/ProviderPage/profileupdate', profile)
            .success(function (data, status, headers, config) {
                console.log(data);
                if (data == "updated") {
                    bootbox.alert("<h4 style='color:rgba(53, 118, 34, 0.83)'><i class='fa fa-check fa-3x'></i>&nbsp;&nbsp;&nbsp;Profile Updated successfully</h4>");
                } 
                else if (data.error || data == "error") {
                    console.log("error:" + data.error);
                }
            })
            .error(function (data, status, headers, config) {
                bootbox.alert("<h4 style='color:red'><i class='fa fa-check fa-3x'></i>&nbsp;&nbsp;&nbsp;Unable to Update [Server Issue]</h4>");
            })
        }
        
        // New course submission
        $scope.submitcourse = function (newcourse) {
            $scope.newcourse.userid = myService.get();
            // TBF unable to bind data with angular[date is not comming from the ejs file]---------------------- SUMIT
            newcourse.Startdate = $('#Startdateadd').val();
            newcourse.Enddate = $('#Enddateadd').val();
            console.log(newcourse);
            $http.post('/api/v1/provider_api/ProviderPage/addcourse', newcourse)
            .success(function (data, status, headers, config) {
                console.log(data);
                if (data == "updated") {
                    bootbox.alert("<span style='color:green;font-size=large'>Course Added Successfully</span>");
                    refresh();
                } 
                else if (data.error) {
                    alert("asdASD");
                }
            })
            .error(function (data, status, headers, config) {
                alert(rejection);
            });
        }
        
        // Click on edit button
        $scope.editcourse = function (courseid) {
            $http.get('/api/v1/provider_api/api/ProviderPage/editcourse/' + courseid).then(function (res) {
                $('#courselist').hide();
                $('#editdataid').show();
                $scope.editdata = res.data[0];
                //console.log(editdata);
                $http.get('/api/v1/course-category_api/api/course_category_items/' + res.data[0].Domain).then(function (response) {
                    $scope.TrainingName = response.data[0].cat_datas.split(',');
                });
            });
        }
        
        // Click on delete button
        $scope.removecourse = function (courseid) {            
            console.log('remove called');
            $http.post('/api/v1/provider_api/ProviderPage/removecourse', { _id: courseid }).success(function (response) {
                $scope.response = response;
                $scope.loading = false;
                bootbox.alert("<span style='color:green;font-size=large'>Course Deleted Successfully</span>");
                refresh();
            });
        }
        
        // Update the course

        $scope.updatecourse = function (item) {
            var res = $http.post('/api/v1/provider_api/ProviderPage/updatecourse', item);
            
            res.success(function (data, status, headers, config) {
                
                bootbox.alert("<h4 style='color:rgba(53, 118, 34, 0.83)'><i class='fa fa-check fa-3x'></i>&nbsp;&nbsp;&nbsp;Updated successfully</h4>");
                
                refresh();
            });
        };
        
        
        $scope.backtolist = function () {
            refresh();
            $('#editdataid').hide();
            $('#courselist').show();
        }
        
        
        $http.get('/api/index').then(function (response) {
            var allitems = response.data;
            var domains = [];
            $.each(allitems, function (index, value) {
                domains.push(value.training_cat);
            });
            
            $scope.domains = domains;
            
            
        });
        
        $scope.getCourses = function (coursename) {
            $http.get('/api/v1/course-category_api/api/course_category_items/' + coursename).then(function (response) {
                $scope.courses = response.data[0].cat_datas.split(",");
                console.log(response.data);
            });
        }
        
        $http.get('/api/indexlogin').then(function (response) {
            
            console.log(response.data.user);
            
            if (response.data.user == "unknown") {
                $('#logindiv').show();
                $('#logout').hide();
            }
            else {
                $scope.LUN = response.data.user;
                $('#logindiv').hide();
                $('#logout').show();
            }
            
            //$scope.hidable = true;
            
            // toggle hide and shoe edit items
            $scope.toggle = function (item) {
                item.show = !item.show;
            }


        });
        
        
        
        /*--------------------- handeling file upload ---------------------------------*/
        
        $scope.log = '';
        console.log($scope.files);
        $scope.upload = function (files) {
            if (files && files.length) {
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    Upload.upload({
                        url: '/api/v1/upload_api/testupload/' + myService.get(),
                        fields: {
                            'username': "asdaSD"
                        },
                        file: file
                    }).progress(function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        $scope.log = 'progress: ' + progressPercentage + '% ' +
                                        evt.config.file.name + '\n' + $scope.log;
                    }).success(function (data, status, headers, config) {
                        $timeout(function () {
                            $scope.log = 'file: ' + config.file.name + ', Response: ' + JSON.stringify(data) + '\n' + $scope.log;
                            
                            refresh();
                        });
                    });
                }
            }
        };
        
    }]);


app.factory('myService', function () {
    var savedData = {}
    function set(data) {
        savedData = data;
    }
    function get() {
        return savedData;
    }
    
    return {
        set: set,
        get: get
    }

});
