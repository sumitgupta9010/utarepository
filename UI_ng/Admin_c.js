﻿var myapp = angular.module("ngadmin", ['ngRoute']);

myapp.controller("Admin_user_controller", ['$scope', '$http', function ($scope, $http) {
        
        var refresh = function () {
            $http.get('/api/v1/admin_api/api/Admin/getAllUsers').then(function (response) {
                $scope.users = response.data;
            });
        };
        refresh();
        
        $scope.removeuser = function (item) {
            var res = $http.post('/api/v1/admin_api/api/Admin/removeuser/' + item);
            console.log(item);
            res.success(function (data, status, headers, config) {
                refresh();
            });
        }
        
        $scope.toggle = function (item) {
            item.show = !item.show;
        }
        
        $scope.updateuserstatus = function (stat, id) {
            var res = $http.post('/api/v1/admin_api/api/Admin/updateuserstatus/' + id, { status: stat });
            res.success(function (data, status, headers, config) {
                if (data === "updated") {
                    
                    bootbox.alert("<h4 style='color:green'><i class='fa fa-check fa-3x'></i>&nbsp;&nbsp;&nbsp;Update successfully</h4>");
                }
            });
        }
    }]);

    // Admin course controller
myapp.controller("Admin_course_controller", ['$scope', '$http', function ($scope,  $http) {

        var refreshcourse = function () {
            $http.get('/api/v1/admin_api/api/Admin/getAllCourse').then(function (response) {
                $scope.courses = response.data;
            });
        }
        refreshcourse();
        
        $http.get('/api/index').then(function (response) {
            var allitems = response.data;
            var domains = [];
            $.each(allitems, function (index, value) {
                domains.push(value.training_cat);
            });            
            $scope.domains = domains;
        });        
        
        $scope.getCourses = function (coursename) {
            $http.get('/api/v1/course-category_api/api/course_category_items/' + coursename).then(function (response) {
                $scope.TrainingNames = response.data[0].cat_datas.split(",");
            });
        }

        $scope.updatecourse = function (item) {


            var res = $http.post('/api/v1/admin_api/api/Admin/updatecourse', item);
           
            res.success(function (data1, status, headers, config) {
                if (data === "updated") {
                    
                    bootbox.alert("<h4 style='color:green'><i class='fa fa-check fa-3x'></i>&nbsp;&nbsp;&nbsp;Update successfully</h4>");
                }
            });
        }
                
        $scope.removecourse = function (item) {
            var res = $http.post('/api/v1/admin_api/api/Admin/removecourse/' + item);
         
            res.success(function (data1, status, headers, config) {
                refreshcourse();
            });
        };
         $scope.toggle = function (item) {
            item.show = !item.show;
            $http.get('/api/v1/course-category_api/api/course_category_items/' + item.Domain).then(function (response) {

                $scope.TrainingNames = response.data[0].cat_datas.split(",");
            });
        }
    }]);
        


// IF REQUIRED TO ADD ANY SERVICE
//app.factory('myService', function () {
//    var savedData = {}
//    function set(data) {
//        savedData = data;
//    }
//    function get() {
//        return savedData;
//    }
    
//    return {
//        set: set,
//        get: get
//    }

//});