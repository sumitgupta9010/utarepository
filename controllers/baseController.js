﻿(function (baseController) {
    var url = require('url');
    var allproviders = [];
    baseController.init = function (app, passport) {
        var trainings_category = require('../app/models/trainings_category');
        var Providers = require('../app/models/providers');
        var ProviderDetails = require('../app/models/ProviderDetails');
        
        app.get('/samepagelogin',  function (req, res) {
            req.session.redirect_to = req.headers.referer;
            res.redirect('/auth/facebook');
          
        });

        // INDEX view page rendering page
        app.get('/', function (req, res) {                            
                res.render('index.ejs');
        });
        // INDEX lOGIN api
        app.get('/api/indexlogin', function (req, res) {
            if (req.session.redirect_to) {

                var path_redirect = req.session.redirect_to;
                req.session.redirect_to = "";
                res.redirect(path_redirect);
            }
            else {
                try {
                    req.session.redirect_to = req.path;
                    var user = req.user._doc.seeker_profile.first_name;
                }

           catch (exception) {
                    var user = "unknown";
                }

                res.send({ 'user': user });
            }
        });
        // INDEX api
        app.get('/api/index', function (req, res) {
            trainings_category.find({}, function (err, docs) {
                if (!err) {

                } else { throw err; }
                res.send(JSON.stringify(docs));

            });
        });

        //-----------------------------------------------------------------------------------------------------------------------------------
        
        // COURSE VIEW PAGE TO SEE THE COURSE
        // SEARCH WILL NAVIGATE HERE
        
        app.get('/course_category/:course_cat_name', function (req, res) {

            res.render('course_category.ejs', { course_cat_name: req.params.course_cat_name });

        });       


//-----------------------------------------------------------------------------------------------------------------------------------

        // ===========Institute page navigations================= FOR VIEWING THE DETAILED PAGE OF THE COURSE ==============
        app.get('/InstitutePage/:instiId', function (req, res) {

            res.render('InstitutePage.ejs', { instiId: req.params.instiId });

        });

        app.get('/api/InstitutePage/:instiId', function (req, res) {

            userasProvider.find({ providerid: req.params.instiId }, function (err, docs) {
                if (!err) {
                } else { throw err; }
                res.send(JSON.stringify(docs));

            });
        });


        app.get('/login', function (req, res) {
            res.render('login.ejs')
        });

//====================================== post provider login -----------------GETTING PROVIDER DETAILS AND COURSE DETAILS ---------

        app.get('/ProviderPage', IsLoggedInToLogin , function (req, res,next) {
            //next(error);
            var data = req.user._doc.userid;

            res.render('ProviderPage.ejs', { 'userid': data });
        });       

        // ===========================================  Admin portion=================================

        app.get('/Admin', function (req, res) {

            res.render('Admin.ejs');

        });
            

        //========================== search =========================================
        

        //========================================= AWS ============================================
        
    };

    //------------------ loggedin ------------------------------------------
    
    function tologsame(req, res, next) { 
        if (req.isAuthenticated())
            return next();
        req.session.redirect_to = req.path;
    }
     
    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated())
            return next();
        res.redirect('/');
    }
    function IsLoggedInToLogin(req, res, next) {
        if (req.isAuthenticated())
            return next();
        req.session.redirect_to = req.path;
        res.redirect('/login');
    }
    
})(module.exports);