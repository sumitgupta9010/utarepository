﻿(function (controllers) {
    var baseController = require("./baseController");
    controllers.init = function (app, passport) {
        baseController.init(app,passport);
    };
})(module.exports);