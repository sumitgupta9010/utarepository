// load all the things we need
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy   = require('passport-google-oauth').OAuth2Strategy;
var LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;
var LocalStrategy    = require('passport-local').Strategy;

var Const = require('./constants.js');
// load up the user model
var User = require('../app/models/user');

// load the auth variables
var configAuth = require('./auth'); // use this one for testing

module.exports = function(passport) {

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    passport.use(new FacebookStrategy({

        clientID        : configAuth.facebookAuth.clientID,
        clientSecret    : configAuth.facebookAuth.clientSecret,
        callbackURL     : configAuth.facebookAuth.callbackURL,
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    },
    function(req, token, refreshToken, profile, done) {

        // asynchronous
        process.nextTick(function() {

            // check if the user is already logged in
            if (!req.user) {

                User.findOne({ 'email' : (profile.emails[0].value || '').toLowerCase() }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {

                        // if there is a user id already but no token (user was linked at one point and then removed)
                        if (!user.token) {
                            user.token = token;
                            user.seeker_profile.first_name = profile.name.givenName;
                            user.seeker_profile.last_name = profile.name.familyName;
                            user.email = (profile.emails[0].value || '').toLowerCase();
                            user.gender = profile.gender;
                            user.save(function(err) {
                                if (err)
                                    return done(err);
                                    
                                return done(null, user);
                            });
                        }

                        return done(null, user); // user found, return that user
                    } else {
                        // if there is no user, create them
                        var newUser            = new User();

                        newUser.userid    = profile.id;
                        newUser.token = token;
                        newUser.loyalty_points = 500;
                        newUser.role = "user";
                        newUser.seeker_profile.first_name = profile.name.givenName;
                        newUser.seeker_profile.last_name = profile.name.familyName;
                        newUser.email = (profile.emails[0].value || '').toLowerCase();
                        newUser.gender = profile.gender;
                        newUser.authSource = "facebook";
                        var mydate = new Date();
                        newUser.InitDate = mydate.toDateString();
                        User.count({}, function (err, c) {
                            newUser.counterId = c;
                            newUser.providerid = (profile.emails[0].value || '').toLowerCase();                            
                            newUser.save(function (err) {
                                if (err)
                                    return done(err);
                                
                                return done(null, newUser);
                            });
                        });
                        
                        //newUser.providerid = "";
                       
                    }
                });

            } else {
                // user already exists and is logged in, we have to link accounts
                var user            = req.user; // pull the user out of the session

                user.userid    = profile.id;
                user.token = token;
                user.seeker_profile.first_name = profile.name.givenName;
                user.seeker_profile.last_name = profile.name.familyName;
                user.email = (profile.emails[0].value || '').toLowerCase();
                user.authSource = "facebook";
                user.save(function(err) {
                    if (err)
                        return done(err);
                        
                    return done(null, user);
                });

            }
        });

    }));


    // =========================================================================
    // LinkedIn ================================================================
    // =========================================================================
    passport.use(new LinkedInStrategy({

            clientID        : configAuth.linkedinAuth.clientID,
            clientSecret    : configAuth.linkedinAuth.clientSecret,
            callbackURL     : configAuth.linkedinAuth.callbackURL,
            scope:        [ 'r_basicprofile', 'r_emailaddress'],
            passReqToCallback          : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

        },
        function(req, accessToken, refreshToken, profile, done) {

            req.session.accessToken = accessToken;
            // asynchronous
            process.nextTick(function() {

                // check if the user is already logged in
                if (!req.user) {

                    User.findOne({ 'email' : (profile.emails[0].value || '').toLowerCase() }, function(err, user) {
                        if (err)
                            return done(err);

                        if (user) {

                            // if there is a user id already but no token (user was linked at one point and then removed)
                            if (!user.token) {
                                user.token = accessToken;
                                user.seeker_profile.first_name = profile.name.givenName;
                                user.seeker_profile.last_name = profile.name.familyName;
                                user.email = (profile.emails[0].value || '').toLowerCase();
                                user.gender = profile.gender;
                                user.save(function(err) {
                                    if (err)
                                        return done(err);

                                    return done(null, user);
                                });
                            }

                            return done(null, user); // user found, return that user
                        } else {
                            // if there is no user, create them
                            var newUser            = new User();

                            newUser.userid    = profile.id;
                            newUser.token = accessToken;
                            //newUser.loyalty_points = 500;
                            //newUser.role = Const.roles.abby;
                            newUser.seeker_profile.first_name = profile.name.givenName;
                            newUser.seeker_profile.last_name = profile.name.familyName;
                            newUser.email = (profile.emails[0].value || '').toLowerCase();
                            newUser.gender = profile.gender;
                            newUser.authSource = "linkedin";
                            newUser.save(function(err) {
                                if (err)
                                    return done(err);

                                return done(null, newUser);
                            });
                        }
                    });

                } else {
                    // user already exists and is logged in, we have to link accounts
                    var user            = req.user; // pull the user out of the session

                    user.userid    = profile.id;
                    user.token = accessToken;
                    user.seeker_profile.first_name = profile.name.givenName;
                    user.seeker_profile.last_name = profile.name.familyName;
                    user.email = (profile.emails[0].value || '').toLowerCase();
                    user.authSource = "linkedin";
                    user.save(function(err) {
                        if (err)
                            return done(err);

                        return done(null, user);
                    });

                }
            });

        }));


     // =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================
    passport.use(new GoogleStrategy({

        clientID        : configAuth.googleAuth.clientID,
        clientSecret    : configAuth.googleAuth.clientSecret,
        callbackURL     : configAuth.googleAuth.callbackURL,
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    },
    function(req, token, refreshToken, profile, done) {

        // asynchronous
        process.nextTick(function() {

            // check if the user is already logged in
            if (!req.user) {

                User.findOne({ 'email' : (profile.emails[0].value || '').toLowerCase() }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {

                        // if there is a user id already but no token (user was linked at one point and then removed)
                        if (!user.token) {
                            user.userid = profile.id,
                            user.token = token;
                            user.seeker_profile.first_name = profile.name.givenName;
                            user.seeker_profile.last_name = profile.name.familyName;
                            user.email = (profile.emails[0].value || '').toLowerCase(); // pull the first email
                            user.authSource = "google";
                            user.save(function(err) {
                                if (err)
                                    return done(err);
                                    
                                return done(null, user);
                            });
                        }

                        return done(null, user);
                    } else {
                        var newUser          = new User();
                        newUser.userid    = profile.id;
                        newUser.token = token;
                        newUser.loyalty_points = 500;
                        //newUser.role = Const.roles.abby;
                        newUser.seeker_profile.first_name = profile.name.givenName;
                        newUser.seeker_profile.last_name = profile.name.familyName;
                        newUser.email = (profile.emails[0].value || '').toLowerCase(); // pull the first email
                        newUser.authSource = "google";
                        newUser.save(function(err) {
                            if (err)
                                return done(err);
                                
                            return done(null, newUser);
                        });
                    }
                });

            } else {
                // user already exists and is logged in, we have to link accounts
                var user               = req.user; // pull the user out of the session
                user.userid    = profile.id;
                user.token = token;
                user.seeker_profile.first_name = profile.name.givenName;
                user.seeker_profile.last_name = profile.name.familyName;
                user.email = (profile.emails[0].value || '').toLowerCase(); // pull the first email
                user.authSource = "google";
                user.save(function(err) {
                    if (err)
                        return done(err);
                        
                    return done(null, user);
                });
            }
        });
    }));

};
