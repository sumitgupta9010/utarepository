﻿var app = angular.module("nginstitutepage", ['ngRoute']);

app.controller("institutepage_controller", ['$scope', 'myService', '$http', function ($scope, myService, $http) {
        console.log("get request is up");
        
        
        // function callme() {
        $http.get('/api/institutepage/' + $('#instiId').text()).then(function (response) {
            $scope.data = response.data;                
        });
    }]);


app.factory('myService', function () {
    var savedData = {}
    function set(data) {
        savedData = data;
    }
    function get() {
        return savedData;
    }
    
    return {
        set: set,
        get: get
    }

});