﻿var myapp = angular.module("ngadmin", ['ngRoute']);

myapp.controller("Admin_controller", ['$scope', '$http', function ($scope,  $http) {
       // console.log("get request is up");       
        
        
        var refresh = function () {
            
            $scope.getAllUsers = function () {
                $('#usertable').show();
                $('#coursetable').hide();
                $scope.usershow = true;
                $scope.courseshow = false;
                $http.get('/api/Admin/getAllUsers').then(function (response) {
                    $scope.users = response.data;
                //console.log(data);
              
                });
            };
        };
        refresh();

        var refreshcourse = function () {
            
            $scope.getAllCourse = function () {
                $('#usertable').hide();
                $('#coursetable').show();
                $scope.usershow = false;
                $scope.courseshow = true;
                
                $http.get('/api/Admin/getAllCourse').then(function (response) {
                    $scope.courses = response.data;
                //console.log(data);
              
                });

            }
        };
        refreshcourse();
        
        //====================== update ==================================
        
        

        $scope.updatecourse = function (item) {
            var res = $http.post('/api/Admin/updatecourse', item);
            
            res.success(function (data1, status, headers, config) {
                // $scope.message = data1;
                timesupdated++;
                console.log(data1);
                if (timesupdated < 2) {
                    $('#formupdated').html("Data has been updated successfully");
                }
                else {
                    $('#formupdated').html(timesupdated + "Times data has been updated successfully");
                    //refresh();
                    item.show = true;
                }
            });
        };
        
        
        // ===========================Delete==============================

        $scope.removeuser = function (item) {
            var res = $http.post('/api/Admin/removeuser/' + item);
            console.log(item);      
            res.success(function (data1, status, headers, config) {               
                refresh();
            });
        };
        
        $scope.removecourse = function (item) {
            var res = $http.post('/api/Admin/removecourse/' + item);
            console.log(item);             
            res.success(function (data1, status, headers, config) {
                refreshcourse();
            });
        };
        

        
        $scope.toggle = function (item) {
            item.show = !item.show;
        }

        $("#mytempform").hide();
        $('#hideme').click(function (){
            $("#mytempform").toggle();
        });
    }]);


