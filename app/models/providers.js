﻿//mongoose
var mongoose = require('mongoose');

var providerSchema = mongoose.Schema({
    name: String,
    address: String,
    tag : String,
    summary : String

});

module.exports = mongoose.model('providers', providerSchema);