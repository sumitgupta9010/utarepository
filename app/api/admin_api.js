﻿
var app = require('express').Router();


var userasProvider = require('../../app/models/user');
var allcourses = require('../../app/models/allcourse');
var trainings_category = require('../../app/models/trainings_category');

app.get('/api/Admin/getAllUsers', function (req, res) {
    
    userasProvider.find({}, function (err, docs) {
        if (!err) {
        } else { throw err; }
        res.send(JSON.stringify(docs));

    });
});

app.post('/api/Admin/removeuser/:id', function (req, res) {
    
    var userid = (req.body);
    // var ppp = userasProvider.find({ "userid": id });
    allcourses.update({ _id: userid }, { Status : 'Deleted' }, function (err) {
        if (err)
            console.log('error' + err);
        else {
            res.send("updated");
            console.log('success');
        }
    });
});

app.post('/api/Admin/updateuserstatus/:id', function (req, res) {
    
    var id = req.params.id;
    var status = req.body.status.toString();
    console.log(status);
    userasProvider.update({_id:id}, { $set: { Status: status }}, function (err, docs) {
        if (!err) {
        } else { throw err; }
        res.send("updated");

    });
});

// ---------------------------- courses ---------------------------------

app.get('/api/Admin/getAllCourse', function (req, res) {
    
    allcourses.find({}, function (err, docs) {
        if (!err) {
        } else { throw err; }
        res.send(JSON.stringify(docs));

    });
});

app.post('/api/Admin/updatecourse', function (req, res) {
    
    var course = (req.body);
    // var ppp = userasProvider.find({ "userid": id });
    allcourses.update({ _id: course._id }, course, function (err) {
        if (err)
            console.log('error' + err);
        else {
            res.send("updated");
            console.log('success');
        }
    });
});

// ----------- remove


app.post('/api/Admin/removecourse/:id', function (req, res) {
    
    var courseid = req.params.id;
    // var ppp = userasProvider.find({ "userid": id });
    allcourses.update({ _id: courseid }, { Deleted : true }, function (err) {
        if (err)
            console.log('error' + err);
        else {
            res.send("updated");
            console.log('success');
        }
    });
});



module.exports = app;