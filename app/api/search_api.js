﻿var app = require('express').Router();


var userasProvider = require('../../app/models/user');
var allcourses = require('../../app/models/allcourse');
var trainings_category = require('../../app/models/trainings_category');

function buildResultSet(docs) {
    var result = [];
    for (var object in docs) {
        result.push(docs[object]);
    }
    return result;
}

app.get('/search', function (req, res) {
    var regex = new RegExp(req.query["term"], 'i');
    var query = allcourses.find({ TrainingName: regex }, { 'TrainingName': 1 }).limit(5);
    
    // Execute query in a callback and return users list
    query.exec(function (err, allcourse) {
        if (!err) {
            // Method to construct the json result set
            //var result = buildResultSet(allcourse);
            
            var result = buildResultSet(allcourse);
            res.send(result, {
                'Content-Type': 'application/json'
            }, 200);
        } else {
            res.send(JSON.stringify(err), {
                'Content-Type': 'application/json'
            }, 404);
        }
    });
});


module.exports = app;